package br.com.improving.carrinho;

/**
 * Classe que representa um produto que pode ser adicionado como item ao
 * carrinho de compras.
 *
 * Importante: Dois produtos são considerados iguais quando ambos possuem o
 * mesmo código.
 */
public class Produto implements Comparable<Produto> {

	private Long codigo;
	private String descricao;

	/**
	 * Construtor da classe Produto.
	 *
	 * @param codigo
	 * @param descricao
	 */
	public Produto(Long codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	/**
	 * Retorna o código da produto.
	 *
	 * @return Long
	 */
	public Long getCodigo() {
		return this.codigo;
	}

	/**
	 * Retorna a descrição do produto.
	 *
	 * @return String
	 */
	public String getDescricao() {
		return this.descricao;
	}

	@Override
	public int compareTo(Produto o) {
		// TODO Auto-generated method stub
//		Se for maior retorna positivo
		if (this.codigo > o.codigo){
			return 1;
		}
//		Se for menor retorna -1
		if (this.codigo < o.codigo){
			return -1;
		}
		return 0;
	}
}