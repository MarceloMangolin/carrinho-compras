package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe que representa o carrinho de compras de um cliente.
 */
public class CarrinhoCompras implements Comparable<CarrinhoCompras> {

	private static int nroItens = 0;
	private String identificacaoCliente = "";
	private List<Item> itens = new LinkedList<>();

	/**
	 * Permite a adição de um novo item no carrinho de compras.
	 *
	 * Caso o item já exista no carrinho para este mesmo produto, as seguintes
	 * regras deverão ser seguidas: - A quantidade do item deverá ser a soma da
	 * quantidade atual com a quantidade passada como parâmetro. - Se o valor
	 * unitário informado for diferente do valor unitário atual do item, o novo
	 * valor unitário do item deverá ser o passado como parâmetro.
	 *
	 * Devem ser lançadas subclasses de RuntimeException caso não seja possível
	 * adicionar o item ao carrinho de compras.
	 *
	 * @param produto
	 * @param valorUnitario
	 * @param quantidade
	 */

	CarrinhoCompras(String identificacaoCliente) {
		this.identificacaoCliente = identificacaoCliente;
	}

	public void adicionarItem(Produto produto, BigDecimal valorUnitario, int quantidade) {

		// cria um item para comparação
		Item itRecebido = new Item(produto, valorUnitario, quantidade);

		// verifica se já existe o produto
		itens.forEach(item -> {
			if (produto.equals(item)) {
				// Encontrou o produto
				// itRecebido.somaProduto(item.getQuantidade());
				// itRecebido.alteraValorUnitario(valorUnitario);
				item.somaProduto(itRecebido.getQuantidade());
				item.alteraValorUnitario(itRecebido.getValorUnitario());
			} else {
				// Adiciona produto
				adicionaItem(itRecebido);
			}
		});

		// itens.stream()
		// .filter(s->s.contains(produto))
		// .foreach(System.out::println);

	}

	/**
	 * Permite a remoção do item que representa este produto do carrinho de
	 * compras.
	 *
	 * @param produto
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no
	 *         carrinho de compras e false caso o produto não exista no
	 *         carrinho.
	 */
	public boolean removerItem(Produto produto) {

		// procurar item
		Item itRecebido = new Item(produto);

		for (int i = 0; i < itens.size(); i++) {
			if (itens.equals(itRecebido)) {
				removeItem(itRecebido);
				return true;
			}
		}

		// itens.forEach(item -> {
		// if (produto.equals(item)) {
		// // Encontrou o produto
		// removeItem(itRecebido);
		//// result = true; XXXXXXXXXXXXXXXX preciso resolver esta linha
		// XXXXXXXXXXXXXXXXXXXXXX
		// }
		// });
		return false;
	}

	/**
	 * Permite a remoção do item de acordo com a posição. Essa posição deve ser
	 * determinada pela ordem de inclusão do produto na coleção, em que zero
	 * representa o primeiro item.
	 *
	 * @param posicaoItem
	 * @return Retorna um boolean, tendo o valor true caso o produto exista no
	 *         carrinho de compras e false caso o produto não exista no
	 *         carrinho.
	 */
	public boolean removerItem(int posicaoItem) {

		if (posicaoItem <= itens.size()) {
			removeItem(posicaoItem);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Retorna o valor total do carrinho de compras, que deve ser a soma dos
	 * valores totais de todos os itens que compõem o carrinho.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTotal() {
		BigDecimal total = new BigDecimal(0);
		itens.forEach(item -> total.add(item.getValorTotal()));
		return total;
	}

	/**
	 * Retorna a lista de itens do carrinho de compras.
	 *
	 * @return itens
	 */
	public Collection<Item> getItens() {
		return Collections.unmodifiableList(this.itens);
	}

	/**
	 * Recebe um objeto Item e Adiciona um item na lista e controla o nr total
	 * de itens
	 * 
	 */
	private void adicionaItem(Item item) {
		CarrinhoCompras.nroItens++;
		itens.add(item);
	}

	/**
	 * Recebe um nr de índice de item Remove da lista o índice da lista controla
	 * o nr total de itens
	 * 
	 */
	private void removeItem(int item) {
		CarrinhoCompras.nroItens--;
		itens.remove(item);
	}

	/**
	 * Recebe um ojeto Item Remove da lista o Item da lista controla o nr total
	 * de itens
	 * 
	 */
	private void removeItem(Item item) {
		CarrinhoCompras.nroItens--;
		itens.remove(item);
	}

	@Override
	public int compareTo(CarrinhoCompras o) {
		// Se for maior igual
		if (this.identificacaoCliente == o.identificacaoCliente) {
			return 0;
		} else {
			return -1;
		}
	}

}