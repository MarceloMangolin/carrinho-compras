package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */

public class CarrinhoComprasFactory {
	
	private static BigDecimal nroCarrinhos= new BigDecimal(0);
	private List<CarrinhoCompras> carrinhos = new LinkedList<>();
	
    /**
     * Cria e retorna um novo carrinho de compras para o cliente passado como parâmetro.
     *
     * Caso já exista um carrinho de compras para o cliente passado como parâmetro, este carrinho deverá ser retornado.
     *
     * @param identificacaoCliente
     * @return CarrinhoCompras
     */
	
    public CarrinhoCompras criar(String identificacaoCliente) {

    	// cria um item para comparação
    	CarrinhoCompras cc = new CarrinhoCompras(identificacaoCliente);
		boolean result = false;
    	
		// verifica se já existe o carrinho
    	
		for (int i = 0; i < carrinhos.size(); i++) {
			if (carrinhos.equals(cc)){
				//Se já existe retorna o carrinho existente 
				cc=carrinhos.get(i);
				result = true;
				break;
			}
		}
    	
		if (!result) {
			// se não exsiste adiciona e retorna o carrinho
			nroCarrinhos.add(new BigDecimal(1));
			carrinhos.add(cc);	
		}
		
		return cc;
    }
    
    /**
     * Retorna o valor do ticket médio no momento da chamada ao método.
     * O valor do ticket médio é a soma do valor total de todos os carrinhos de compra dividido
     * pela quantidade de carrinhos de compra.
     * O valor retornado deverá ser arredondado com duas casas decimais, seguindo a regra:
     * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
     *
     * @return BigDecimal
     */
    public BigDecimal getValorTicketMedio() {

    	BigDecimal ticketMedio= new BigDecimal(0);

		carrinhos.forEach(carrinho->ticketMedio.add(carrinho.getValorTotal().divide(nroCarrinhos, 2)));
		return ticketMedio;    	
    }

    /**
     * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessão expirar.
     * Deve ser efetuada a remoção do carrinho do cliente passado como parâmetro da listagem de carrinhos de compras.
     *
     * @param identificacaoCliente
     * @return Retorna um boolean, tendo o valor true caso o cliente passado como parämetro tenha um carrinho de compras e
     * e false caso o cliente não possua um carrinho.
     */
    public boolean invalidar(String identificacaoCliente) {

		// procurar carrinho
		CarrinhoCompras carrinho = new CarrinhoCompras(identificacaoCliente);

		for (int i = 0; i < carrinhos.size(); i++) {
			if (carrinhos.equals(carrinho)){
				carrinhos.remove(i);
				return true;
			}
		}
		
		return false;    		
    	
    }
}
